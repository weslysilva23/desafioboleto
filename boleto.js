function modulo11(codigoBarra) {

    var codigoBarra = codigoBarra.replace(/[^\d]+/g, '');

    var soma = 0;
    var fator = 2;

    for (var i = codigoBarra.length - 1; i >= 0; i--) {
        if(i != 4){
            soma += (codigoBarra.charAt(i) * fator);
            if (fator == 9)
                fator = 2;
            else
                fator++;
        }
    }

    var resto = soma % 11;
    var digito = 11 - resto;

    if (digito > 9){
        digito = 0;
    }

    codigoBarra = codigoBarra.substring(0,4) + digito + codigoBarra.substring(5, codigoBarra.length);
    return Promise.resolve(codigoBarra)
}




function linhaDigitavel(idBoleto) {
    
    return new Promise(async function(resolve, reject) {
        
        if(!idBoleto.match(/^[0-9]+$/)){
            reject("Boleto inválido, informe apenas numeros")
        }

        if(idBoleto.length >= 48){
                    
            modulo11(idBoleto).then(validacao=>{
            
                if(validacao){
                    
                    try {
                        var baseDay = new Date("07/10/1997");
                    
                        var codigo = validacao.replace(/[^\d]+/g, '');
                        var dataVencimento = codigo.substring(22, 26);
                        var valor = `${parseInt(codigo.substring(12, 14))},${parseInt(codigo.substring(14, 16))}`;
                        var fatorVencimento = codigo.substring(0, 3);
                        let fator = parseInt(fatorVencimento) * (1000 * 3600 * 24 )
                        dataVencimento = new Date(baseDay.getTime() + fator)
                        dataVencimento = `${dataVencimento.getFullYear()}/${dataVencimento.getMonth()+1}/${dataVencimento.getDate()+1}`

    
                        
                            resolve({
                                amout: valor,
                                expirantionDate: dataVencimento,
                                barCode: validacao,
                            })

                    } catch (error) {
                        console.error(error)
                        reject("Problema na geração da linha digitavel")
                    }
                   
                }else{
                    reject("Codigo de barras inválido")
                }
            })

        
        }else if(idBoleto.length == 44){
                                
            modulo11(idBoleto).then(validacao=>{
            
                if(validacao){

                    try {

                        var baseDay = new Date("10/07/1997");
                        var codigo = validacao.replace(/[^\d]+/g, '');
                        var valor = `${parseInt(codigo.substring(10, 17))},${parseInt(codigo.substring(18, 19))}`;
                        var fatorVencimento = codigo.substring(5, 9);
                        let fator = parseInt(fatorVencimento) * (1000 * 3600 * 24 )
                        dataVencimento = new Date(baseDay.getTime() + fator)
                        dataVencimento = `${dataVencimento.getFullYear()}/${dataVencimento.getMonth()+1}/${dataVencimento.getDate()+1}`

                        resolve({
                            amout: valor,
                            expirantionDate: dataVencimento,
                            barCode: validacao,
                        })
                        

                    } catch (error) {
                        console.error(error)
                        reject("Problema na geração da linha digitavel")

                    }
                   
                }
            
            }).catch(err=>{
                reject(err)
            })
    
            }else{
                reject("Codigo de barras inválido")
            }
        })

}


module.exports = {
    linhaDigitavel,
}
