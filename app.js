var express = require('express');
var app = express();
var bodyParser = require('body-parser');
const boleto = require('./boleto.js');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static('public'));

app.get('/', function (req, res) {
  res.send('Hello, world!');
});

app.get('/boleto/:id', function (req, res) {

    var id = req.params.id;
    boleto.linhaDigitavel(id).then(result=>{
        
        res.json(result);

    }).catch(err=>{
        
        res.json(err);
    
    })


});

app.listen(8080, function () {
  console.log('Example app listening on port 8080!');
});


