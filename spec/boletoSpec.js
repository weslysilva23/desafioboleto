
const axios = require('axios');

describe("Teste Boleto", function() {

    describe("Teste de Servidor", function() {
        
        //fazer uma requisicao do tipo get para a url http://localhost:3000/boleto/titulo
        it("Servidor esta rodando ?", function(done) {

            axios.get('http://localhost:8080/')
                .then(function(response){
                   expect(response.status).toBe(200);
                    done();
                })
                .catch(function(error){
                    done(error);
                })
        });
    });

    describe("Teste Titulo", function() {
        
        //fazer uma requisicao do tipo get para a url http://localhost:3000/boleto/titulo
        it("Deve retornar um titulo", function(done) {

            axios.get('http://localhost:8080/boleto/38398879900027710000009241190000002536815871')
                .then(function(response){
                    expect(response.data.amout).toEqual("27710,0")
                    expect(response.data.expirantionDate).toEqual("2021/11/9")
                    expect(response.data.barCode).toEqual("38398879900027710000009241190000002536815871")
                    done();
                })
                .catch(function(error){
                    done(error);
                })
        });
    });

    describe("Teste Titulo", function() {
        
        //fazer uma requisicao do tipo get para a url http://localhost:3000/boleto/titulo
        it("Deve retornar um titulo", function(done) {

            axios.get('http://localhost:8080/boleto/83800000000-9 44560030000-0 01606540410-2 11263382473-9')
                .then(function(response){
                    expect(response.data.amout).toEqual("44,56")
                    expect(response.data.expirantionDate).toEqual("2021/07/23")
                    expect(response.data.barCode).toEqual("838000000009445600300000016065404102112633824739")
                    done();
                })
                .catch(function(error){
                    done(error);
                })
        });
    });
    
});

