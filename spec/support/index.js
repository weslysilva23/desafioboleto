const path =  require('path');
process.env.rootDir = path.join(__dirname, '');
const config = require('../config.json')
process.env.configJson = JSON.stringify(config)

var Jasmine = require('jasmine');
var jasmine = new Jasmine();

require('../app.js');
jasmine.loadConfigFile('spec/support/jasmine.json');
jasmine.env.clearReporters();
jasmine.execute();